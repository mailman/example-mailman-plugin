from mailman.core.i18n import _
from mailman.interfaces.command import ContinueProcessing, IEmailCommand
from mailman.utilities.options import I18nCommand
from public import public
from zope.interface import implementer


@public
@implementer(IEmailCommand)
class MyEmailCommand():
    """A command that sends an email."""

    name = 'myemail'
    description = 'Example email command.'

    def process(mlist, msg, msgdata, arguments, results):
        return ContinueProcessing.yes
